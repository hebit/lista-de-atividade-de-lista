#ifndef _LSE
#define _LSE
#include <stdbool.h>
/* A nossa função */

typedef struct Node
{
  float value;
  struct Node* next;
} node;

typedef struct
{
  node* begin;
  int size;
} list;

void printList(list L);

bool createList(list* L);

bool clearList(list* L);

bool checkListSorting(list L);

node* allocNode(float x);

bool push_back(list* L, float x);

bool push_front(list* L, float x);

bool push_sorted(list* L, float x);

void concatLists(list* L1, list* L2);

bool removeFirst(list* L);

node* getLastListItem(list* L);

list* mergeLists(list L1, list L2);

#endif
