#include <stdio.h>
#include <stdlib.h>
#include "LSE.h"

int main(){
  int N;
  float x;
  list Lista1, Lista2;
  list* Lista3;
  createList(&Lista1);
  createList(&Lista2);
  scanf("%d", &N);
  for(int i = 0; i < N; i++){
    //printf("%d°:\n",i);
    // scanf("%f", &x);
    x = rand() % 100;
    push_sorted(&Lista1, x);
    x = rand() % 50;
    push_sorted(&Lista2, x);
  }
  
  Lista3 = mergeLists(Lista1, Lista2);
  printList(*(Lista3));
  return 0;
}
