#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "LSE.h"

void printList(list L){
  node* aux = L.begin;
  while(aux != NULL){
    printf("%.1f ", aux->value);
    aux = aux->next;
  }printf("\n");
  return;
}

bool createList(list* L){
  L->begin = NULL;
  L->size = 0;
  return true;
}

bool clearList(list* L){
  while (L->begin != NULL) {
    free(L->begin);
    L->size--;
    L->begin = L->begin->next;
  }
  if(L->size == 0)
    return true;

  return false;
}

bool checkListSorting(list L){
  node* aux = L.begin;
  if(aux == NULL)
    return true;
  while(aux->next != NULL){
    if(aux->value > aux->next->value)
      return false;
    aux = aux->next;
  }
  return true;
}

node* allocNode(float x){
  node* elem;
  elem = (node*) malloc(sizeof(node));
  if(elem == NULL){
    return NULL;
  }
  elem->value = x;
  elem->next = NULL;
  return elem;
}

bool push_back(list* L, float x){
  node* aux = L->begin;
  node* last = allocNode(x);

  if(last == NULL)
    return false;
  if(aux == NULL){
      L->begin = last;
      L->size++;
      return true;
  }
  /*aqui*/
  while(aux->next != NULL){
    aux = aux->next;
  }
  aux->next = last;
  L->size++;
  return true;
}

bool push_front(list* L, float x){
  node* aux = L->begin;
  node* first = allocNode(x);

  if(first == NULL)
    return false;
  first->next = aux;
  L->begin = first;
  L->size++;
  return true;
}

bool push_sorted(list* L, float x){
  node* aux = L->begin;
  node* elem = allocNode(x);

  if(elem == NULL)
    return false;
  if(aux == NULL || x < L->begin->value)
    return push_front(L,x);


  while(aux->next != NULL && aux->next->value <= x){
    aux = aux->next;
  }
  /*
    node* before = aux;
    node* after = aux->next;
    before->next = elem;
    elem->next = after;
  */
  elem->next = aux->next;
  aux->next = elem;
  return true;
}

void concatLists(list* L1, list* L2){
  if(L1->begin == NULL){
    list* Laux = L1;
    L1 = L2;
    L2 = Laux;
  }
  if(L1->begin == NULL || L2->begin == NULL){
    return;
  }
  /*aqui*/
  node* aux;
  aux = L1->begin;
  while(aux->next != NULL){
    aux = aux->next;
  }

  aux->next = L2->begin;
  L1->size += L2->size;
  L2->begin = NULL;
  return;
}

bool removeFirst(list* L){
  if(L->begin == NULL)
    return false;

  node* old_first = L->begin;
  L->begin = L->begin->next;
  L->size--;
  free(old_first);
  return true;
}

node* getLastListItem(list* L){
  node* aux = L->begin;
  if(aux == NULL)
    return NULL;
  while(aux->next != NULL){
    aux = aux->next;
  }
  return aux;
}

list* mergeLists(list L1, list L2){
  list* L3 = (list*) malloc(sizeof(list));
  createList(L3);
  if(!checkListSorting(L1) || !checkListSorting(L2))
    return L3;

  while(L1.begin != NULL && L2.begin != NULL){
    if(L1.begin->value < L2.begin->value){
      push_back(L3, L1.begin->value);
      removeFirst(&L1);
    }else{
      push_back(L3, L2.begin->value);
      removeFirst(&L2);
    }
    L3->size++;
  }
  node* last = getLastListItem(L3);
  if(L1.begin == NULL){
    last->next = L2.begin;
  }else{
    last->next = L1.begin;
  }
  return L3;

}
